source /usr/share/defaults/etc/profile

# My terminal
export MYTERM='alacritty'

# Alacritty
source $HOME/.config/alacritty/alacritty_bash_completion

# Rust
PATH="$PATH:$HOME/.cargo/bin"
source $HOME/.cargo/env

# Go
export GOPATH="$HOME/go"
PATH="$PATH:$GOPATH/bin"

# Open independent terminal in current directory                        # synthax is made for Alacritty
alias th='$MYTERM --working-directory $(pwd) </dev/null &>/dev/null &'  # might not work with every terminal

# History size
export HISTFILESIZE=20000
export HISTSIZE=10000
